This module adds Dropbox File Chooser to your File or Image fields.

No dependencies other than core file module.

To configure the module you will have to create a Drop-in app (https://www.dropbox.com/developers/apps/create?app_type_checked=dropins).
Read more about the Dropbox Chooser here (https://www.dropbox.com/developers/dropins/chooser/js).

There are two steps you have to complete in order to enable the module:

1. Once you create a Drop-in app locate your Dropbox App Key and add it to the module. The configuration page is located here: `admin/config/services/dropbox-filefield`.

2. Edit your File or Image field and enable Dropbox File Chooser.