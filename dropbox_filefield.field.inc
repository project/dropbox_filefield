<?php

/**
 * @file
 * Dropbox file field.
 */

/**
 * Implements hook_field_info_alter().
 */
function dropbox_filefield_field_info_alter(&$info) {
  foreach (array('file', 'image') as $type) {
    if (isset($info[$type])) {
      $info[$type]['instance_settings'] += array(
        'dropbox_filefield_enabled' => FALSE,
      );
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for field_ui_field_edit_form().
 */
function dropbox_filefield_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (dropbox_filefield_field_widget_support($form['#instance']['widget']['type'])) {
    $settings = $form['#instance']['settings'];

    $additions['dropbox_filefield_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dropbox'),
      '#collapsible' => TRUE,
      '#collapsed' => !$settings['dropbox_filefield_enabled'],
      '#parents' => array('instance', 'settings'),
      '#weight' => 20,
    );

    $additions['dropbox_filefield_settings']['dropbox_filefield_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value' => $settings['dropbox_filefield_enabled'],
      '#description' => t('Enable Dropbox File Chooser. Read more about the <a href="!link" target="_blank">Dropbox JavaScript Chooser API</a>',
        array(
          '!link' => 'https://www.dropbox.com/developers/dropins/chooser/js',
        )
      ),
    );

    $form['instance']['settings'] += $additions;
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function dropbox_filefield_field_widget_form_alter(&$element, &$form_state, $context) {
  $instance = $context['instance'];
  if (!empty($instance['settings']['dropbox_filefield_enabled']) && dropbox_filefield_field_widget_support($instance['widget']['type'])) {
    $keys = element_children($element);
    $delta = end($keys);

    $cardinality = $context['field']['cardinality'];
    $element[$delta]['#dropbox_filefield_max_files'] = $cardinality != FIELD_CARDINALITY_UNLIMITED ? $cardinality - $delta : -1;

    $upload_validators = $element[$delta]['#upload_validators'];
    $upload_validators['file_validate_size'] = array(dropbox_filefield_max_filesize($instance));
    $element[$delta]['#dropbox_filefield_upload_validators'] = $upload_validators;

    $element[$delta]['#process'][] = 'dropbox_filefield_field_widget_process';
    $element[$delta]['#file_value_callbacks'][] = 'dropbox_filefield_field_widget_value';
  }
}

/**
 * #process callback for the field widget element.
 */
function dropbox_filefield_field_widget_process($element, &$form_state, $form) {
  $path = drupal_get_path('module', 'dropbox_filefield');
  $max_files = $element['#dropbox_filefield_max_files'];

  // Get the upload validators and build a new description.
  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);
  $description = $field['cardinality'] == 1 ? field_filter_xss($instance['description']) : '';
  $upload_validators = $element['#dropbox_filefield_upload_validators'];
  $description = theme('file_upload_help', array('description' => $description, 'upload_validators' => $upload_validators));

  $dropbox_app_key = variable_get('dropbox_filefield_app_key', '');
  $html_element = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="' . $dropbox_app_key . '"></script>' . "\r\n",
  );
  drupal_add_html_head($html_element, 'dropbox_dropin');

  // Add the extension list as a data attribute.
  $extensions = array();
  if (isset($upload_validators['file_validate_extensions'][0])) {
    foreach (array_filter(explode(' ', $upload_validators['file_validate_extensions'][0])) as $ext) {
      $extensions[] = '.' . $ext;
    }
  }

  $multiselect = ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED);

  $element['dropbox_filefield'] = array(
    '#type' => 'hidden',
    '#value_callback' => 'dropbox_filefield_value',
    '#field_name' => $element['#field_name'],
    '#field_parents' => $element['#field_parents'],
    '#upload_location' => $element['#upload_location'],
    '#dropbox_filefield_upload_validators' => $upload_validators,
    '#prefix' => '<div class="dropbox-filefield-wrapper"><a href="#" class="button browse"><span>' . t('Browse') . '</span></a>
    <a href="#" class="button dropbox-chooser" data-description="' . strip_tags($description) . '" data-max-filesize="' . $upload_validators['file_validate_size'][0] . '" data-multiselect="' . $multiselect . '" data-file-extentions="' . join(",", $extensions) . '"><span>' . t('Dropbox') . '</span></a>',
    '#suffix' => '</div>',
    '#attached' => array(
      'css' => array($path . '/dropbox_filefield.css'),
      'js' => array(
        $path . '/dropbox_filefield.js',
      ),
    ),
  );

  $element['upload_button']['#submit'][] = 'dropbox_filefield_field_widget_submit';
  $element['#pre_render'][] = 'dropbox_filefield_field_widget_pre_render';

  return $element;
}

/**
 * #file_value_callbacks callback for the field widget element.
 */
function dropbox_filefield_field_widget_value($element, &$input, $form_state) {
  if (!empty($input['dropbox_filefield'])) {
    $file_urls = explode('|', $input['dropbox_filefield']);
    foreach ($file_urls as $file_url) {
      if ($file = dropbox_filefield_save_upload($element, $file_url)) {
        $input['fid'] = $file->fid;
      }
    }
  }
}

/**
 * #value_callback callback for the dropbox_filefield element.
 */
function dropbox_filefield_value($element, $input = FALSE, $form_state = array()) {
  $fids = array();
  if ($input) {
    $file_urls = explode('|', $input);
    array_shift($file_urls);
    foreach ($file_urls as $file_url) {
      if ($file = dropbox_filefield_save_upload($element, $file_url)) {
        $fids[] = $file->fid;
      }
    }
  }
  return implode(',', $fids);
}

/**
 * #submit callback for the upload button of the field widget element.
 */
function dropbox_filefield_field_widget_submit($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $items = $field_state['items'];

  // Remove possible duplicate items.
  $fids = array();
  foreach ($items as $delta => $item) {
    if (in_array($item['fid'], $fids)) {
      unset($items[$delta]);
    }
    else {
      $fids[] = $item['fid'];
    }
  }
  $items = array_values($items);

  // Append our items.
  if (!empty($element['dropbox_filefield']['#value'])) {
    $fids = array_diff(explode(',', $element['dropbox_filefield']['#value']), $fids);
    foreach ($fids as $fid) {
      $items[] = array('fid' => $fid);
    }
  }

  drupal_array_set_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2), $items);
  $field_state['items'] = $items;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
}

/**
 * #pre_render callback for the field widget element.
 */
function dropbox_filefield_field_widget_pre_render($element) {
  if (!empty($element['#value']['fid'])) {
    $element['dropbox_filefield']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Get the file size limit for a field instance.
 */
function dropbox_filefield_max_filesize($instance) {
  $max_filesize = file_upload_max_size();

  if (!empty($instance['settings']['max_filesize'])) {
    $size = parse_size($instance['settings']['max_filesize']);
    if ($size < $max_filesize) {
      $max_filesize = $size;
    }
  }

  return $max_filesize;
}

/**
 * Check whether our module has support for a widget type.
 */
function dropbox_filefield_field_widget_support($widget_type) {
  return $widget_type == 'file_generic' || $widget_type == 'image_image';
}


/**
 * Get file information and its contents to upload.
 */
function dropbox_filefield_file_info($path) {
  $file = pathinfo($path);

  $finfo = @finfo_open(FILEINFO_MIME_TYPE);
  $mimetype = @finfo_file($finfo, $path);
  $contents = file_get_contents($path);

  $info = array(
    'filename'  => $file['basename'],
    'extension' => $file['extension'],
    'mimetype'  => $mimetype,
    'filesize'  => strlen($contents)
  );
  return (object) $info;
}

/**
 * Save a completed upload.
 */
function dropbox_filefield_save_upload($element, $file_url) {
  global $user;

  if (empty($file_url)) {
    return FALSE;
  }

  // Ensure the destination is still valid.
  $destination = $element['#upload_location'];
  $destination_scheme = file_uri_scheme($destination);
  if (!$destination_scheme) {
    return FALSE;
  }

  $remote_file = system_retrieve_file($file_url, $destination);
  $upload = dropbox_filefield_file_info(drupal_realpath($remote_file));

  // Begin building the file object.
  $file = new stdClass();
  $file->uid = $user->uid;
  $file->status = 0;
  $file->filename = trim(drupal_basename($upload->filename), '.');
  $file->uri = $remote_file;
  $file->filemime = file_get_mimetype($file->filename);
  $file->filesize = $upload->filesize;

  // Support Transliteration.
  if (module_exists('transliteration') && variable_get('transliteration_file_uploads', TRUE)) {
    $orig_filename = $file->filename;
    $file->filename = transliteration_clean_filename($file->filename);
  }

  // Munge the filename.
  $validators = $element['#dropbox_filefield_upload_validators'];
  $extensions = '';
  if (isset($validators['file_validate_extensions'])) {
    if (isset($validators['file_validate_extensions'][0])) {
      $extensions = $validators['file_validate_extensions'][0];
    }
    else {
      unset($validators['file_validate_extensions']);
    }
  }
  else {
    $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
    $validators['file_validate_extensions'][] = $extensions;
  }
  if (!empty($extensions)) {
    $file->filename = file_munge_filename($file->filename, $extensions);
  }

  // Rename potentially executable files.
  if (!variable_get('allow_insecure_uploads', 0) && preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->uri .= '.txt';
    $file->filename .= '.txt';
    if (!empty($extensions)) {
      $validators['file_validate_extensions'][0] .= ' txt';
      drupal_set_message(t('For security reasons, your upload has been renamed to %filename.', array('%filename' => $file->filename)));
    }
  }

  // Get the upload element name.
  $element_parents = $element['#parents'];
  if (end($element_parents) == 'dropbox_filefield') {
    unset($element_parents[key($element_parents)]);
  }
  $form_field_name = implode('_', $element_parents);

  // Run validators.
  $validators['file_validate_name_length'] = array();
  $errors = file_validate($file, $validators);
  if ($errors) {
    $message = t('The specified file %name could not be uploaded.', array('%name' => $file->filename));
    if (count($errors) > 1) {
      $message .= theme('item_list', array('items' => $errors));
    }
    else {
      $message .= ' ' . array_pop($errors);
    }
    form_set_error($form_field_name, $message);
    return FALSE;
  }

  // Prepare the destination directory.
  if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
    watchdog('dropbox_filefield', 'The upload directory %directory for the file field !name could not be created or is not accessible. A newly uploaded file could not be saved in this directory as a consequence, and the upload was canceled.', array('%directory' => $destination, '!name' => $element['#field_name']));
    form_set_error($form_field_name, t('The file could not be uploaded.'));
    return FALSE;
  }

  // Complete the destination.
  if (substr($destination, -1) != '/') {
    $destination .= '/';
  }
  $destination = file_destination($destination . $file->filename, FILE_EXISTS_RENAME);

  // Move the uploaded file.
  $file->uri = $destination;
  if (!rename($remote_file, $file->uri)) {
    form_set_error($form_field_name, t('File upload error. Could not move uploaded file.'));
    watchdog('dropbox_filefield', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $file->filename, '%destination' => $file->uri));
    return FALSE;
  }

  // Set the permissions on the new file.
  drupal_chmod($file->uri);

  // Transliteration support: restore the original filename if configured so.
  if (isset($orig_filename) && !variable_get('transliteration_file_uploads_display_name', TRUE)) {
    $file->filename = $orig_filename;
  }

  // Save the file object to the database.
  $file = file_save($file);
  if (!$file) {
    return FALSE;
  }

  return $file;
}
