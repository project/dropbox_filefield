(function ($) {

  /**
   * Dropbox file upload chooser.
   */
  Drupal.behaviors.DropboxFileChooser = {
    attach: function (context, settings) {

      // Hide Upload element. Replace it with custom options.
      $('.form-type-managed-file', context).each(function() {
        if ($(this).find('.dropbox-filefield-wrapper').length) {
          $(this).find('input[type=file]').hide();
          $(this).find('input[type=submit]').hide();
          $(this).find('input[name*=remove]').show();
        }
      });

      // Trigger file upload browser
      $('.form-type-managed-file a.browse', context).unbind().click(function(e) {
        var $parent = $(this).closest('.form-managed-file');
        $parent.find('input[type=file]').click();
        e.preventDefault();
      });

      // Dropbox file chooser.
      $('.form-type-managed-file a.dropbox-chooser', context).unbind().click(function(e) {
        var $parent = $(this).parent().parent();
        var _max_filesize = $(this).data('max-filesize');
        var _description = $(this).data('description');
        var _extensions = $(this).data('file-extentions');
        var _multiselect = $(this).data('multiselect');
        Dropbox.choose({
          success: function(files) {
            var _links = [];
            for (var i = 0; i < files.length; i++) {
              if (files[i].bytes > _max_filesize) {
                alert(_description);
              }
              else {
                _links.push(files[i].link);
              }
            }
            $parent.find('.dropbox-filefield-wrapper input[type=hidden]').val(_links.join('|'));
            $('input.form-submit[value=Upload]', $parent).mousedown();
          },
          extensions: _extensions.split(','),
          multiselect: _multiselect,
          linkType: 'direct'
        });
        e.preventDefault();
      });

      $('.form-type-managed-file input.form-file', context).change(function() {
        var $parent = $(this).parent().parent();
        if ($parent.find('.dropbox-filefield-wrapper').length) {
          setTimeout(function() {
            if(!$('.error', $parent).length) {
              $('input.form-submit[value=Upload]', $parent).mousedown();
            }
          }, 100);
        }
      });

    }
  };

}(jQuery));
